//
//  File.swift
//  debbie-code-challenge
//
//  Created by Huy Nguyen on 5/19/17.
//  Copyright © 2017 Huy Nguyen. All rights reserved.
//

import Foundation

class Post {
    var id: Int?
    var message: String?
    var user: User?
    var postedAt: String?
    var imageURL: String?
    var likeCount: Int?
    var commentCount: Int?
    
    init(data: NSDictionary) {
        if let id = data.value(forKey: Constants.Obj.ID) as? Int {
            self.id = id
        }
        
        if let user = data.value(forKey: Constants.Obj.USER) as? NSDictionary {
            self.user = User(data: user)
        }
        
        if let message = data.value(forKey: Constants.Obj.Post.MESSAGE) as? String {
            self.message = message
        }
        
        if let postedAt = data.value(forKey: Constants.Obj.Post.POST_AT) as? String {
            self.postedAt = postedAt
        }
        
        if let imageURL = data.value(forKey: Constants.Obj.IMAGE_URL) as? String {
            self.imageURL = imageURL
        }
        
        if let likeCount = data.value(forKey: Constants.Obj.DATA) as? Int {
            self.likeCount = likeCount
        }
        
        if let commentCount = data.value(forKey: Constants.Obj.DATA) as? Int {
            self.commentCount = commentCount
        }
    }
}
