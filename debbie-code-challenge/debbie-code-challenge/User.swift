//
//  User.swift
//  debbie-code-challenge
//
//  Created by Huy Nguyen on 5/19/17.
//  Copyright © 2017 Huy Nguyen. All rights reserved.
//

import Foundation

class User {
    var _id: Int?
    var fullName: String?
    var mobileNo: String?
    var dob: String?
    var imageURL: String?
    
    init(data: NSDictionary) {
        if let _id = data.value(forKey: Constants.Obj.ID) as? Int {
            self._id = _id
        }
        
        if let fullName = data.value(forKey: Constants.Obj.User.FULL_NAME) as? String {
            self.fullName = fullName
        }
        
        if let mobileNo = data.value(forKey: Constants.Obj.User.MOBILE) as? String {
            self.mobileNo = mobileNo
        }
        
        if let dob = data.value(forKey: Constants.Obj.User.DOB) as? String {
            self.dob = dob
        }
        
        if let imageURL = data.value(forKey: Constants.Obj.IMAGE_URL) as? String {
            self.imageURL = imageURL
        }
    }
}
