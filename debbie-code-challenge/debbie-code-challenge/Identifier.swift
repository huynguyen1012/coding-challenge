//
//  Identifier.swift
//  debbie-code-challenge
//
//  Created by Huy Nguyen on 5/19/17.
//  Copyright © 2017 Huy Nguyen. All rights reserved.
//

import Foundation
import UIKit

protocol Identifier {
    static var identifierView: String {get}
    static func xib() -> UINib?
}
