//
//  FetchImageRequest.swift
//  debbie-code-challenge
//
//  Created by Huy Nguyen on 5/19/17.
//  Copyright © 2017 Huy Nguyen. All rights reserved.
//
import UIKit
import Foundation

class ImageDownloader {
    
    var cache = NSCache<AnyObject, AnyObject>()
    
    class var sharedLoader : ImageDownloader {
        struct Static {
            static let instance : ImageDownloader = ImageDownloader()
        }
        return Static.instance
    }
    
    func image(forURL urlString: String, completionHandler:@escaping (_ image: UIImage?, _ url: String) -> ()) {
        DispatchQueue.global(qos: DispatchQoS.QoSClass.background).async(execute: {()in
            let data: Data? = self.cache.object(forKey: urlString as AnyObject) as? Data
            if let goodData = data {
                let image = UIImage(data: goodData)
                DispatchQueue.main.async(execute: {() in
                    completionHandler(image, urlString)
                })
                return
            }
            
            let downloadTask: URLSessionDataTask = URLSession.shared.dataTask(with: URL(string: urlString)!, completionHandler: { (data, response, error) -> Void in
                if (error != nil) {
                    completionHandler(nil, urlString)
                    return
                }
                
                if data != nil {
                    let image = UIImage(data: data!)
                    self.cache.setObject(data! as AnyObject, forKey: urlString as AnyObject)
                    DispatchQueue.main.async(execute: {() in
                        completionHandler(image, urlString)
                    })
                    return
                }
            })
            downloadTask.resume()
        })
        
    }
}
