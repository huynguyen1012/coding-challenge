//
//  NetworkOperation.swift
//  debbie-code-challenge
//
//  Created by Huy Nguyen on 5/20/17.
//  Copyright © 2017 Huy Nguyen. All rights reserved.
//

import UIKit

public typealias JSONResults = [String: AnyObject]

class NetworkOperation: AsynchronousOperation {

    let url: URL
    let session: URLSession
    let requestCompletionHandler: (Data?, URLResponse?, Error?) -> ()
    
    init(session: URLSession, url: URL, requestCompletionHandler: @escaping(Data?, URLResponse?, Error?) -> ()) {
        self.url = url
        self.session = session
        self.requestCompletionHandler = requestCompletionHandler
        
        super.init()
    }
    
    private weak var task: URLSessionTask?
    
    override func main() {
        let task = session.dataTask(with: url) { data, response, error in
            self.requestCompletionHandler(data, response, error)
            self.completeOperation()
        }
        task.resume()
        self.task = task
    }
    
    override func cancel() {
        task?.cancel()
        super.cancel()
    }
}
