//
//  TrendCell.swift
//  debbie-code-challenge
//
//  Created by Huy Nguyen on 5/18/17.
//  Copyright © 2017 Huy Nguyen. All rights reserved.
//

import UIKit

class TrendCell: UITableViewCell {

    @IBOutlet weak var avatarImg: UIImageView!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var coverImg: UIImageView!
    @IBOutlet weak var messageLbl: UILabel!
    @IBOutlet weak var likeLbl: UILabel!
    @IBOutlet weak var commentLbl: UILabel!
    
    fileprivate let imageDownloader = ImageDownloader()
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    
    func configCell(post: Post) {
        if let avatarURL = post.user?.imageURL, let coverImgURL = post.imageURL {
            imageDownloader.image(forURL: avatarURL, completionHandler: { (image, error) in
                self.avatarImg.image = image
            })
            
            imageDownloader.image(forURL: coverImgURL, completionHandler: { (image, error) in
                self.coverImg.image = image
            })
        }
        if let name = post.user?.fullName {
            nameLbl.text = name
        }
        
        if let message = post.message {
            messageLbl.text = message
        }
        
        if let likeCount = post.likeCount {
            likeLbl.text = "\(likeCount)"
        }
        
        if let cmtCount = post.commentCount {
            commentLbl.text = "\(cmtCount)"
        }
    }
    
    func getMoreInfo(post: Post, indexPath: IndexPath) {
        guard let _id = post.id else { return }
        let queue = OperationQueue()
        queue.name = "com.turtle.debbie-code-challenge.info"
        
        let commentCountURL = URL(string: "\(Constants.MAIN_URL)\(Constants.KeyPath.POST)/\(_id)\(Constants.KeyPath.COMMENT_COUNT)")!
        let getCmtOper = NetworkOperation(session: URLSession.shared, url: commentCountURL) { data, response, error in
            guard let data = data, error == nil else {
                return
            }
            var response: Any? = nil
            do {
                response = try JSONSerialization.jsonObject(with: data, options: [])
            } catch {
                print("exception decoding api data")
            }
            
            guard let result = response as? JSONResults, let _commentCount = result["data"] as? Int else {
                return
            }
            
            post.commentCount = _commentCount
        }
        
        let likeCountURL = URL(string: "\(Constants.MAIN_URL)\(Constants.KeyPath.POST)/\(_id)\(Constants.KeyPath.LIKE_COUNT)")!
        let getLikeOper = NetworkOperation(session: URLSession.shared, url: likeCountURL) { data, response, error in
            guard let data = data, error == nil else {
                return
            }
            var response: Any? = nil
            do {
                response = try JSONSerialization.jsonObject(with: data, options: [])
            } catch {
                print("exception decoding api data")
            }
            guard let result = response as? JSONResults, let likeCount = result["data"] as? Int else {
                return
            }
            post.likeCount = likeCount
        }
        
        getLikeOper.addDependency(getCmtOper)
        
        queue.addOperations([getCmtOper, getLikeOper], waitUntilFinished: true)
    }
}
