//
//  ViewController.swift
//  debbie-code-challenge
//
//  Created by Huy Nguyen on 5/18/17.
//  Copyright © 2017 Huy Nguyen. All rights reserved.
//

import UIKit

class TrendViewController: UIViewController {
    
    var posts = [Post]()
    @IBOutlet weak var tableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.fetchPost()
        self.tableView.register(UINib(nibName: "TrendCell", bundle: nil), forCellReuseIdentifier: "TrendCell")
//        self.tableView.contentInset = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
    }
    
    private func fetchPost() {
        let queue = OperationQueue()
        queue.name = "com.turtle.debbie-code-challenge.post"
        
        let getPostURL = URL(string: Constants.MAIN_URL + Constants.KeyPath.POST)!
        let getPostOper = NetworkOperation(session: URLSession.shared, url: getPostURL) { data, response, error in
            guard let data = data, error == nil else {
                return
            }
            var response: Any? = nil
            do {
                response = try JSONSerialization.jsonObject(with: data, options: [])
            } catch {
                print("exception decoding api data")
            }
            
            guard let result = response as? JSONResults, let arrPost = result["data"] as? NSArray else {
                return
            }
            arrPost.forEach {
                if let postDic = $0 as? NSDictionary {
                    let post = Post(data: postDic)
                    self.posts.append(post)
                    self.tableView.reloadData()
                }
            }
        }
        queue.addOperations([getPostOper], waitUntilFinished: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

extension TrendViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TrendCell", for: indexPath) as! TrendCell
        cell.getMoreInfo(post: posts[indexPath.row], indexPath: indexPath)
        cell.configCell(post: posts[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return posts.count
    }
}

