//
//  AsynchronousOperation.swift
//  debbie-code-challenge
//
//  Created by Huy Nguyen on 5/20/17.
//  Copyright © 2017 Huy Nguyen. All rights reserved.
//

import Foundation

class AsynchronousOperation: Operation {
    
    override var isAsynchronous: Bool { return true }
    
    private let stateLock = NSLock()
    
    private var _excuting: Bool = false
    override var isExecuting: Bool {
        get {
            return stateLock.withCriticalScope { _excuting }
        } set {
            willChangeValue(forKey: "isExecuting")
            stateLock.withCriticalScope { _excuting = newValue }
            didChangeValue(forKey: "isExecuting")
        }
    }
    
    private var _finished: Bool = false
    override var isFinished: Bool {
        get {
            return stateLock.withCriticalScope{ _finished }
        } set {
            willChangeValue(forKey: "isFinished")
            stateLock.withCriticalScope{ _finished = newValue }
            didChangeValue(forKey: "isFinished")
        }
    }
    
    public func completeOperation() {
        if isExecuting {
            isExecuting = false
        }
        
        if !isFinished {
            isFinished = true
        }
    }
    
    override func start() {
        if isCancelled {
            isFinished = true
            return
        }
        isExecuting = true
        main()
    }
}

extension NSLock {
    
    func withCriticalScope<T>(block: () -> T) -> T {
        lock()
        let value = block()
        unlock()
        return value
    }
}
