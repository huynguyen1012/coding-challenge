//
//  Constants.swift
//  debbie-code-challenge
//
//  Created by Huy Nguyen on 5/19/17.
//  Copyright © 2017 Huy Nguyen. All rights reserved.
//

import Foundation

struct Constants {
    
    static let MAIN_URL = "http://thedemoapp.herokuapp.com"
    
    struct KeyPath {
        static let POST = "/post"
        static let LIKE_COUNT = "/likeCount"
        static let COMMENT_COUNT = "/commentCount"
    }
    
    struct Obj {
        static let ID = "_id"
        static let IMAGE_URL = "imageURL"
        static let USER = "user"
        static let DATA = "data"
        
        struct Post {
            static let MESSAGE = "message"
            static let POST_AT = "postedAt"
        }
        
        struct User {
            static let FULL_NAME = "fullName"
            static let MOBILE = "mobileNo"
            static let DOB = "dob"
        }
    }
}
