//
//  UITableView+Register.swift
//  debbie-code-challenge
//
//  Created by Huy Nguyen on 5/19/17.
//  Copyright © 2017 Huy Nguyen. All rights reserved.
//

import Foundation
import UIKit

extension UITableView {
    func registerCell<T: Identifier>(_ viewType: T.Type) {
        self.register(viewType.xib(), forCellReuseIdentifier: viewType.identifierView)
    }
}

