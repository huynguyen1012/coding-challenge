//
//  main.m
//  codingchallenge-ios-objc
//
//  Created by Tang Han on 19/2/16.
//  Copyright © 2016 User Experience Research. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
